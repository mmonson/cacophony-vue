var express = require('express')
var path = require('path')
var serveStatic = require('serve-static')
var spawn = require('child_process').spawn;

var builder = spawn('yarn', ['run', 'build'], { detached: true})

let app = express()
app.use(serveStatic(path.join(__dirname, 'dist')))
app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'dist', 'index.html')))
var port = process.env.PORT || 5050
app.listen(port)
console.log('server started ' + port)
