import axios from 'axios'
import appState from '../store/app_state'

axios.defaults.withCredentials = true

export default {
  state: {
    hasSession: null
  },
  checkLoggedIn (callback, obj) {
    axios
      .get(appState.server_url + '/api/authenticate')
      .then((response) => {
        this.state.hasSession = response.status === 200
        appState.state.user_id = parseInt(response.data.user_id)
      })
      .catch((error) => {
        this.state.hasSession = error.response.status === 200
        appState.state.user_id = null
      })
      .then(() => {
        console.log('session: ' + this.state.hasSession)
        if (callback && obj) callback(obj)
        return this.state.hasSession
      })
  },

  login (email, password, callback) {
    axios
      .post(appState.server_url + '/api/authenticate', {
        email: email,
        password: password
      })
      .then((response) => {
        callback(response)
      })
      .catch((error) => {
        console.log(error)
        callback(error.response)
      })
  },

  register (username, password, email, callback) {
    axios
      .post(appState.server_url + '/api/register', {
        username: username,
        email: email,
        password: password
      })
      .then((response) => {
        callback(response)
      })
      .catch((error) => {
        console.log(error)
        callback(error.response)
      })
  },

  logout (callback, obj) {
    console.log('logout')
    axios
      .delete(appState.server_url + '/api/authenticate')
      .then((response) => {
        console.log(response)
        this.state.hasSession = response.status !== 200
      })
      .catch((error) => {
        this.state.hasSession = error.response.status !== 200
      })
      .then(() => {
        setTimeout(this.checkLoggedIn(), 0)
        callback(obj)
        return this.state.hasSession
      })
  }
}
