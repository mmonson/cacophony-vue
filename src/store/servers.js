import axios from 'axios'
import appState from './app_state'
import MessageStore from './messages'

axios.defaults.withCredentials = true

export default {
  state: {
    /** List of servers available to the user. */
    servers: [],
    /** A list of channels for the currently selected server. */
    channels: []
  },
  updateServers (callback = null, obj = null) {
    axios
      .get(appState.server_url + '/api/user/server_list')
      .then((response) => {
        if (response.data) {
          console.log(response.data)
          this.state.servers = response.data.servers
          let activeServer = appState.state.active_server = this.state.servers[0]
          this.getChannelList(activeServer.id, () => {
            MessageStore.loadInitMessages(appState.state.active_server, appState.state.active_channel)
          }, null)

          if (callback && obj) {
            callback(obj)
          }
        }
      })
      .catch((error) => {
        console.log(error)
        console.log(error.reponse)
      })
  },
  createServer (name, callback, obj) {
    axios
      .post(appState.server_url + '/api/manage_server', {
        server_name: name
      })
      .then((response) => {
        if (response.status === 201) {
          callback(obj, 201)
        }
      })
      .catch((error) => {
        callback(obj, error.response.status)
      })
  },
  getChannelList (serverID, callback, obj) {
    console.log(serverID)
    axios
      .get(appState.server_url + '/api/server/channel', {
        params: {
          server_id: serverID
        }
      })
      .then((response) => {
        if (response.status === 200) {
          this.state.channels = response.data.channels
          appState.state.active_channel = this.state.channels[0]
          if (callback && obj) {
            callback(obj)
          }
        }
      })
      .catch((error) => {
        this.state.channels = []
        appState.state.active_channel = null
        console.log(error, error.response)
      })
  },
  createChannel (serverID, name, callback, obj) {
    axios
      .post(appState.server_url + '/api/channel', {
        server_id: serverID,
        channel_name: name
      })
      .then((response) => {
        if (response.status === 201) {
          callback(obj, 201)
        }
      })
      .catch((error) => {
        console.log(error)
        if (callback && obj) {
          callback(obj, error.reponse.status)
        }
      })
  },
  // joinServer (serverID, callback, obj) {
  //   axios
  //     .post(appState.server_url + '/api/invite', {
  //       server_id: serverID
  //     })
  //     .then((response) => {
  //       if (response.status === 200) {
  //         callback(obj)
  //       }
  //     })
  //     .catch((error) => {
  //       console.error(error)
  //     })
  // }
  joinServer (serverID, callback, obj) {
    axios
      .get(appState.server_url + '/api/invite', {
        params: {
          server_id: serverID
        }
      })
      .then((response) => {
        if (response.status === 200) {
          callback(obj)
        }
      })
      .catch((error) => {
        console.error(error)
      })
  }
}
