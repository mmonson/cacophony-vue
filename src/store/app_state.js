import io from 'socket.io-client'
import MessageStore from './messages'

const IS_PRODUCTION = process.env.NODE_ENV === 'production'

export default {
  server_url: IS_PRODUCTION ? 'https://cacophony-flask.herokuapp.com' : 'http://localhost:5000',
  state: {
    active_server: null,
    active_channel: null,
    socket: null,
    user_id: null,
    socketConnected: false,
    rooms: []
  },
  computed: {
    connected () {
      return this.socket.connected
    }
  },
  startSocket () {
    this.state.socket = io(this.server_url, {
      transports: ['websocket']
    })
    this.state.socket.on('connect', () => {
      this.state.socketConnected = true
      console.log('Socket ready.')
    })
    this.state.socket.on('message', (data) => {
      console.log(data)
      MessageStore.receiveMessage(data)
    })
    this.state.socket.on('message-delete', (data) => {
      MessageStore.removeMessage(data)
    })
    this.state.socket.on('disconnect', () => {
      this.state.socketConnected = false
      this.state.socket.emit('disconnected')
    })
  }
}
