import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueChatScroll from 'vue-chat-scroll'

import { library } from '@fortawesome/fontawesome-svg-core'
// import { fas } from '@fortawesome/fontawesome-free-solid'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import './filters'

library.add(faCoffee)
// library.add(fas)
Vue.use(VueChatScroll)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
  // components: { App },
  // template: '<App/>',
  // currentView: 'App'
}).$mount('#app')
